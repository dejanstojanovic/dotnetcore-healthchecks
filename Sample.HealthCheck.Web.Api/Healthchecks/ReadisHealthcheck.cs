﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Sample.HealthCheck.Web.Api.Healthchecks
{
    public class ReadisHealthcheck : IHealthCheck
    {
        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default(CancellationToken))
        {
            using (ConnectionMultiplexer redis = ConnectionMultiplexer.Connect("localhost:6379"))
            {
                try
                {
                    var db = redis.GetDatabase(0);
                }
                catch (Exception)
                {
                    return await Task.FromResult(HealthCheckResult.Failed());
                }
            }
            return await Task.FromResult(HealthCheckResult.Passed());
        }
    }
}
