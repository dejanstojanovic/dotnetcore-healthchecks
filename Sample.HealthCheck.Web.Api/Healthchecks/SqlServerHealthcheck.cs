﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace Sample.HealthCheck.Web.Api.Healthchecks
{
    public class SqlServerHealthcheck : IHealthCheck
    {
        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default(CancellationToken))
        {
            using (var connection = new SqlConnection("Data Source=.\\SQLEXPRESS;Initial Catalog=ProductsCatalog;Integrated Security=SSPI;"))
            {
                try
                {
                    await connection.OpenAsync();
                }
                catch (Exception)
                {
                    return HealthCheckResult.Failed();
                }
                return HealthCheckResult.Passed();
            }
        }
    }

  


}
