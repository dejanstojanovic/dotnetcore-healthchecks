﻿Param(   
[Parameter(Mandatory=$True, Position=1)]
[string]$packages
)

Foreach($package in $packages.Split(' ')){
    $packageCommand = "Install-Package " 
     
    if($package.Split(':').Length -gt 1){
        $packageCommand += $package.Split(':')[0] + " -Version " + $package.Split(':')[1]
    }
    else{
        $packageCommand += $package
    }

    #echo($packageCommand)

    Invoke-Expression $packageCommand

}